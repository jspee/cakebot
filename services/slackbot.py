# simple slackbot for announcing kitchen activity
# pull in libraries and set globals
import os 
import logging
import constants
import slack


class slackBot():
    
    def __init__(self, name):
        
        try:  
            os.environ[constants.slack_token]
        except KeyError: 
            logging.info(constants.slack_token + " was not found in environment")
            exit(1)
        self.slack_client = slack.WebClient(os.environ.get(constants.slack_token))
        self.botName = name
        self.channels = self.listChannels()  
   
    def listChannels(self):
        channels_call = self.slack_client.api_call("channels.list")
        if channels_call['ok']:
            return channels_call['channels']
        return None

    def publishMessage(self, message, requestedChannel, botIcon):
        # check channels exist
        if self.channels:
            # loop through channels and find the id that matches the requested channel name
            for channel in self.channels:
                # send message to the required channel
                if channel['name'] == requestedChannel:
                    logging.debug("text message sent to " + channel['name'] + " (" + channel['id'] + ")")   
                    logging.debug("text message content: " + message)    
                    try:
                        self.slack_client.chat_postMessage(
                        channel=channel['id'],
                        text=message,
                        icon_emoji=botIcon)
                    except:
                        logging.info("ERROR in slack transfer")
        return None

    def postImage(self, filePath, requestedChannel):
        self.slack_client.files_upload(
            channels= '#' + requestedChannel,
            file=filePath
        )
