# this class handles the rss feed for quotes
# text is converted using string operators, I'm sure this could be improved

# import feedparser
import logging

class quote():   
    def __init__(self):
        logging.debug("initialised quote rss reader")
    
    def quote(self):
        # get the forecast
        d = feedparser.parse('http://feeds.feedburner.com/azquotes/quoteoftheday')
        quote = d.entries[0].summary
        quote = quote.replace("\"","")
        author = d.entries[0].title
              
        return(quote, author)