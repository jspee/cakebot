import random
import logging

class gifLibrary():
    # return gifs for cakes
    
    def __init__(self):
        self.cakeGifs = []
        self.populateCakeGifs()
        self.lastCakeGifSent = 0
        self.coffeeGifs = []
        self.populateCoffeeGifs()
        self.lastCoffeeGifSent = 0
        self.noCakeGifs = []
        self.populateNoCakeGifs()
        self.lastNoCakeGifSent = 0
        
    def populateCakeGifs(self):
        self.cakeGifs.append("https://media.giphy.com/media/l0MYN32Z3gLPHlXry/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/l3vRmLrL8MfD9CIkU/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/t4aBVelzr49qw/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/w2vePhiBQoDDi/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/PS66xB5cPXKzS/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/Vz1lPyf1J55vy/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/U5EBHPxhojGV2/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/TFXoCcEemsS1G/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/fcFLXGFbsxj8I/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/3NC1J4n28V5jq/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/ef61oIGVyckY8/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/3o6Zt1CbqzTd36KXYY/source.gif")
        self.cakeGifs.append("https://media.giphy.com/media/NI8fT7ugMo9eU/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/3o6ZsXY8h0l6xcmHe0/giphy.gif")
        self.cakeGifs.append(" https://media.giphy.com/media/JOjGbeFoOPL2/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/3osxY7eI6enqNBo2mQ/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/xThuWcu8nwBhazm0Fi/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/zdFbQtZ4pAmcg/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/7gDWjByySj79K/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/Ph1dH8AdDesKc/giphy.gif")
        self.cakeGifs.append("https://media2.giphy.com/media/VvQvOFqPjZLi/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/l3UcD7vkCptuTGAX6/giphy.gif")
        self.cakeGifs.append("https://media.giphy.com/media/10nh8gk9x4pEjK/giphy.gif")
        
    def populateCoffeeGifs(self):
        self.coffeeGifs.append("https://media.giphy.com/media/3xz2BIXYagz5STg0xi/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/xUySTqtLVfKpWWSO1a/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/26uffErnoIpeQ3PmU/source.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/oj05uAreWGy8U/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/oZEBLugoTthxS/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/26ufcxlBQA6QZW6J2/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/DrJm6F9poo4aA/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/Z6vszQ8Mweukw/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/3oFyDpRagf96Uz9rzO/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/5xaYLxI6riEuY/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/11Lz1Y4n1f2j96/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/3oKIPbdUWjexJveA5q/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/xT4uQbiRzL7TPD6Ipy/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/SFfPv1V1k6BrO/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/XMJQ6XFi9mxpe/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/VpmQvIPgz8XPa/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/3og0IzZuTba39fq0WQ/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/DAfnKv5hd2cCY/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/2S4725t1RuFuo/giphy.gif")
        self.coffeeGifs.append("http://i.giphy.com/l3q2BDVISz6kNRMKQ.gif")
        self.coffeeGifs.append("https://d13yacurqjgara.cloudfront.net/users/901546/screenshots/2485950/character-dribbble.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/NHUONhmbo448/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/U39eLhLsiEYPm/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/7erBV7JsTvPuU/giphy.gif")
        self.coffeeGifs.append("https://media.giphy.com/media/ktC7Yk3p6uozm/giphy.gif")
        self.coffeeGifs.append("http://i.imgur.com/CAH9jiG.gif")
        
    def populateNoCakeGifs(self):
        self.noCakeGifs.append("https://media.giphy.com/media/8ykJ4yAnwgK2I/giphy.gif")
        self.noCakeGifs.append("https://media.giphy.com/media/hmE2rlinFM7fi/giphy.gif")
        self.noCakeGifs.append("https://media.giphy.com/media/jIRPOnUASNsQw/giphy.gif")
        self.noCakeGifs.append("https://giphy.com/gifs/jIRPOnUASNsQw")
        self.noCakeGifs.append("https://media.giphy.com/media/Wvo6vaUsQa3Di/giphy.gif")
        self.noCakeGifs.append("https://media.giphy.com/media/1HIBPhFX8nnag/giphy.gif")
        self.noCakeGifs.append("https://media.giphy.com/media/2rtQMJvhzOnRe/giphy.gif")
        self.noCakeGifs.append("https://media2.giphy.com/media/26his7OHUlHe5LAti/giphy.gif")
        self.noCakeGifs.append("https://media.giphy.com/media/QIhj6TEbD6bmw/giphy.gif")
 
    
    def returnNoCakeGif(self):
        randomGif, self.lastNoCakeGifSent = self.returnRandomGIF(self.noCakeGifs, self.lastNoCakeGifSent)
        return (randomGif)
    
    def returnCakeGif(self):
        randomGif, self.lastCakeGifSent = self.returnRandomGIF(self.cakeGifs, self.lastCakeGifSent)
        return (randomGif)
    
    def returnCoffeeGif(self):   
        randomGif, self.lastCoffeeGifSent = self.returnRandomGIF(self.coffeeGifs, self.lastCoffeeGifSent)
        return (randomGif)
    
    def returnRandomGIF(self, gifArray, lastIndexUsed):
        # pass back a random element for the array, do not return the value last used
        while True:
            selectedGifIndex = random.randint(0,len(gifArray)-1)
            if selectedGifIndex != lastIndexUsed:
                break
        return(gifArray[selectedGifIndex], selectedGifIndex)