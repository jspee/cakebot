import random
import logging

class quipLibrary():
    # return gifs for cakes
    
    def __init__(self):
        self._populateCakeQuips()
        self._populateCoffeeBrewingQuips()
        self._populateCoffeeMadeQuips()
        self._populateCoffeeUnknownAgeQuips()
        self._populateFreshCoffeeQuips()
        self._populateNoCakeQuips()
        self._populateOldCoffeeQuips()
        
    # coffee age request - age unknown
    def _populateCoffeeUnknownAgeQuips(self):
        self.coffeeUnknownAgeQuips = []
        self.coffeeUnknownAgeQuips.append("I don't know how old the coff ee is, i was only turned on a little while ago. I can't know everything")
        self.coffeeUnknownAgeQuips.append("I've got now idea how old the coff ee is, I've only just been turned on.")
        self.coffeeUnknownAgeQuips.append("I've got now idea how old the coff ee is, leave me alone.")
        
     # coffee age request - still brewing
    def _populateCoffeeBrewingQuips(self):
        self.coffeeBrewingQuips = []
        self.coffeeBrewingQuips.append("please be patient. the coff ee is still downloading")
        self.coffeeBrewingQuips.append("you'll have to be patient the coff ee can't drip any faster")
        self.coffeeBrewingQuips.append("i can not make gravity pull the coffee through the filter any quicker. You'll have to wait")
        self.coffeeBrewingQuips.append("not long now")
        
    # fresh coffee made
    def _populateCoffeeMadeQuips(self):
        self.coffeeMadeQuips = []
        self.coffeeMadeQuips.append("Excellent. I will tell slack you made fresh coffee. You really are contributing to staff welfare today!")
        self.coffeeMadeQuips.append("I see what you did there. Well done you")
        self.coffeeMadeQuips.append("Good work")
        self.coffeeMadeQuips.append("You have just done a very good thing")
        self.coffeeMadeQuips.append("I am always watching you")
        self.coffeeMadeQuips.append("Nice. Here is a coffee fact. Coffee stays warm 20 percent longer when you add cream. And that's the way the news goes.")
        self.coffeeMadeQuips.append("Brilliant. Did you know that Beethoven was such an ardent coffee lover that he would count 60 beans per cup before making his brew.")
        self.coffeeMadeQuips.append("Cool, cool, cool. Fact time! Despite the fact that caffeine is a mild diuretic, you do not lose more fluid than you take in by drinking coffee, so it cannot dehydrate you.")
        self.coffeeMadeQuips.append("Delicious coffeeee. Coffee beans are only called beans because of the resemblance. They are actually berries.")
        self.coffeeMadeQuips.append("Hooray. Coffee. Coffee was discovered by a goat herder who noticed that goats became extremely energetic after eating certain berries.")
        self.coffeeMadeQuips.append("Ohhh yeah coffee! Check it. Writer Francois Marie Arouet, better known as Voltaire, reportedly drank 40 to 50 cups of a chocolate-coffee mixture each day.")
        self.coffeeMadeQuips.append("Coffee time! According to a 2018 study by Wallet Hub, New York City has the most coffee shops, coffee houses and cafes per capital. I think Leamington does now.")

    # all the cake has gone
    def _populateNoCakeQuips(self):
        self.noCakeQuips = []
        self.noCakeQuips.append("How sad. I will tell slack that all the treats have gone")
        self.noCakeQuips.append("No more treats. That is sad. still. Worse things happen at sea. Get back to work")
        self.noCakeQuips.append("I hope the empty calories were worth it")
    
    # new cake
    def _populateCakeQuips(self):
        self.cakeQuips = []
        self.cakeQuips.append("How generous. I will take a photograph for slack. It will make the people... excited")
        self.cakeQuips.append("Treats. That is the first bit of good news I have had all week")
        self.cakeQuips.append("Treats. I will tell the people")
        self.cakeQuips.append("Treats. You have made people.. Happy")
        
    # fresh coffee
    def _populateFreshCoffeeQuips(self):
        self.freshCoffeeQuips = []
        self.freshCoffeeQuips.append("Hot and fresh, just how you like it")
        self.freshCoffeeQuips.append("Fresh and fruty, just how you like it")
        self.freshCoffeeQuips.append("perfect, and just what you need")
        self.freshCoffeeQuips.append("you look like you need it")
        
    # old coffee
    def _populateOldCoffeeQuips(self):
        self.oldCoffeeQuips = []
        self.oldCoffeeQuips.append("It's going to taste like burnt rubber, and you know it. Treat yourself to a fresh pot.")
        self.oldCoffeeQuips.append("I would not drink that, even if I had a mouth. Treat yourself to a fresh pot.")
        self.oldCoffeeQuips.append("Even starbucks would throw that one away. Make some fresh")
        self.oldCoffeeQuips.append("Have some dignity and make a fresh pot.")
        self.oldCoffeeQuips.append("Roll up your sleeves and make a fresh pot.")
        self.oldCoffeeQuips.append("All that is waiting for you there is bitterness and regret. Make some fresh")
                               
    # simple return functiona 
    
    def returnRandomQuip(self, quipArray):
        selectedQuip = random.randint(0,len(quipArray)-1)
        return(quipArray[selectedQuip])
    
    def returnOldCoffeeQuip(self):
        return (self.returnRandomQuip(self.oldCoffeeQuips))
    
    def returnCoffeeUnknownAgeQuip(self):
        return (self.returnRandomQuip(self.coffeeUnknownAgeQuips))
 
    def returnCoffeeBrewingQuip(self):
        return (self.returnRandomQuip(self.coffeeBrewingQuips))
    
    def returnCoffeeMadeQuip(self):
        return (self.returnRandomQuip(self.coffeeMadeQuips))

    def returnNoCakeQuip(self):
        return (self.returnRandomQuip(self.noCakeQuips))
        
    def returnCakeQuip(self):
        return (self.returnRandomQuip(self.cakeQuips))
           
    def returnFreshCoffeeQuip(self):
        return (self.returnRandomQuip(self.freshCoffeeQuips))

    
    
 
    
    
    
