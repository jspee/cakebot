# this class handles the rss feed for weather
# text is converted using string operators, I'm sure this could be improved

# import feedparser
import logging

class weather():   
    def __init__(self):
        logging.debug("initialised weather rss reader")
    
    def forecast(self):
        # get the forecast
        d = feedparser.parse('http://open.live.bbc.co.uk/weather/feeds/en/2644737/3dayforecast.rss')
        weatherForecast = d.entries[1].title
        # edit the forecast
        weatherForecast = weatherForecast.replace("Temperature:","Temperature,")
        weatherForecast = weatherForecast.replace(":",".")
        weatherForecast = weatherForecast.replace(u'\xb0', " degrees ")
        weatherForecast = weatherForecast.replace(' C ', " centigrade ")
        # remove the reference to the day
        weatherForecast = weatherForecast[weatherForecast.find(".")+1:]
        # add a prefix about today
        weatherForecast = "Tomorrow, the weather in Leamington Spa will be." + weatherForecast
        # remove everything after the centigrade
        weatherForecast = weatherForecast[:weatherForecast.find("centigrade")+len("centigrade")]
       
        return(weatherForecast)
    
   