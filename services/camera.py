import picamera
import constants
import time
import datetime
import logging

class camera():
    def __init__(self):
        self.camera = picamera.PiCamera()
        self.camera.resolution = (640, 480)
        self.camera.rotation = 270
        
    def takePhoto(self): 
        uniqueFilename = '{:%Y-%m-%d_%H:%M:%S_}'.format(datetime.datetime.now()) + constants.imageFileName
        fullImagePath = constants.imageLocation + uniqueFilename
        try:
            self.camera.capture(fullImagePath)
        except:
            logging.debug("Camera failure")
            fullImagePath = constants.imageLocation + constants.cameraFailFileName
        return(fullImagePath)
