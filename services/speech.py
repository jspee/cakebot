# this class handles all the text to speech methods. 
# text is converted into a temporary wav file, the file is played (at terminal level), the file is deleted (at terminal level)

import logging
import subprocess 

class speech():   
    def __init__(self):
        logging.debug("initialised TTS engine")
        self.audioFile = "temp.wav"
    
    def say(self, text):
        subprocess.call(["espeak", "-w"+self.audioFile, text])
        subprocess.call(["aplay", self.audioFile])
        subprocess.call(["rm", self.audioFile])
       
        return()
    