# load libraries
import constants
import logging
import time
from multiprocessing import Process
from services.gifs import gifLibrary
from services.slackbot import slackBot
from services.server import server
from services.speech import speech
from services.weather import weather
from services.quote import quote
from services.quips import quipLibrary

import time
# load classes dependent on environment
if constants.PCMode:
    # PC hardware
    from stubs.led import led
    from stubs.switch import switch
    from stubs.camera import camera
    
else:
    # pi hardware
    from services.led import led
    from services.switch import switch
    from services.camera import camera
    import RPi.GPIO as GPIO 

class bot():
    
    
    
    def __init__(self, name):
        self.name = name
        self.slackMouth = slackBot("CakeBot")
        self.quips = quipLibrary()

        # setup GPIO for use and initialise the webserver to display images
        if not constants.PCMode:
            GPIO.setmode(GPIO.BCM) 
            self.webServer = server() 
        # set up peripherals and services
        self.ledCoffee = led("coffee", constants.pinCoffeeLED)
        self.ledCake = led("cake", constants.pinCakeLED)
        self.ledWeather = None # led("quote", constants.pinWeatherLED)
        self.ledCoffeeAge = led("coffeeage", constants.pinCoffeeAgeLED)
        self.switchCoffee = switch("coffee",constants.pinCoffeeSwitch)
        self.switchCake = switch("cake",constants.pinCakeSwitch)
        self.switchWeather = None # switch ("quote", constants.pinWeatherSwitch)
        self.switchCoffeeAge = switch ("coffeeage", constants.pinCoffeeAgeSwitch)
        self.gifLibrary = gifLibrary()
        self.botCam = camera()
        self.speechEngine = speech()
        self.weatherService = weather()
        self.quoteService = quote()
        # define bot variables
        self.coffeeTimerFirstRun = True
        self.coffeetimerProcess = None
        self.lidHistoricallyClosed = True # assume the lid is closed
        self.lidCurrentlyClosed = True
        self.speechEngine.say("Cake bot is live")
        self.coffeeMadeTime = None
        
    def run(self):
        
        
        if constants.PCMode:
            # running of a dev environment - run a simple test cycle
            logging.debug('running test cycle on dev environment (hostname does not match cakebot')
            self.handleCoffeeAgeRequest() # unknown age
            self.handleCoffeeAlert() 
            self.handleCoffeeAgeRequest() # still brewing
            self.coffeeMadeTime = self.coffeeMadeTime - (30*60) # make the coffee 30 mins older
            self.handleCoffeeAgeRequest() # fresh coffee
            self.coffeeMadeTime = self.coffeeMadeTime - (50*60) # make the coffee 30 mins older
            self.handleCoffeeAgeRequest() # old coffee

            # self.handleWeatherRequest()
            for counter in range(2):                         
                time.sleep(2)
                self.handleCakeAlert()
            self.speechEngine.say("The PC Mode test cycle has completed")   
        else:
            # running on target hardware - run infinite run loop
            while True:
                # handle coffee switch in coffee maker lid
                self.lidCurrentlyClosed = self.switchCoffee.returnState()
                # if lid state has changed
                if self.lidCurrentlyClosed != self.lidHistoricallyClosed:
                    # check if coffee's been made
                    if self.lidCurrentlyClosed and not self.lidHistoricallyClosed:
                        # lid was open and now is closed - coffee is coming
                        logging.debug('Lid CLosed. Coffee alert triggered')
                        self.handleCoffeeAlert()
                    else:
                        logging.debug('Lid opened.')
                    self.lidHistoricallyClosed = self.lidCurrentlyClosed
                # handle cake switch   
                if self.switchCake.returnState():
                    logging.debug('Cake alert triggered')
                    self.handleCakeAlert()    
#                 # handle quote switch
#                 if self.switchWeather.returnState():
#                     self.handleWeatherRequest()

                # handle requests about the coffee's age
                if self.switchCoffeeAge.returnState():
                    self.handleCoffeeAgeRequest()
                    
                time.sleep(0.05) # prevents CPU thrash
        return()       
    
    def handleCoffeeAlert(self):
        logging.info('Coffee was made')
        # log the time
        self.coffeeMadeTime = time.time()
        if self.coffeeTimerFirstRun:
            self.coffeeTimerFirstRun = False
        else:
            if self.coffeetimerProcess.is_alive():
                # kill the pre-existing coffee timer thread
                logging.debug('Coffee light cycle restarted')
                self.coffeetimerProcess.terminate()      
        # define new coffee timer thread
        self.coffeetimerProcess = Process(target=self.ledCoffee.minuteCountdown, args=(30,))
        self.coffeetimerProcess.daemon=True
        # run the coffee timer thread 
        self.coffeetimerProcess.start()
        logging.debug('Coffee light cycle running in background')
        # thank the user and tell slack
        self.speechEngine.say(self.quips.returnCoffeeMadeQuip())
        # pause to allow speech before starting processor heavy upload task (which cause a stammer)
        time.sleep(8) 
        self.slackMouth.publishMessage(self.gifLibrary.returnCoffeeGif()+'\nSomeone just made *fresh coffee!*', constants.kitchenChannel, ":coffee:")
        
        return()
    
    def handleCakeAlert(self):
        if self.ledCake.returnState():
            # led is already on - turn the light off
            self.ledCake.turnOff()
            # recognise uer
            self.speechEngine.say(self.quips.returnNoCakeQuip())
            # tell kitchen channel that cake is finished
            self.slackMouth.publishMessage(self.gifLibrary.returnNoCakeGif() + '\nAll the treats are gone!', constants.kitchenChannel, ":unamused:")
            # tell general channel that cake is finished - NO GIF
            self.slackMouth.publishMessage('All the treats are gone!', constants.generalChannel, ":unamused:")
        else:
            logging.info('Cake was left')
            # turn on the led
            self.ledCake.turnOn()
            # say thank you
            self.speechEngine.say(self.quips.returnCakeQuip())
            # pause to allow speech before starting processor heavy upload task (which cause a stammer)
            time.sleep(3) 
            # take image
            fullImagePath = self.botCam.takePhoto()
            message = 'There are treats in the kitchen....'
            # tell kitchen channel that cake has arrived - include a gif
            self.slackMouth.publishMessage(self.gifLibrary.returnCakeGif() + '\n' + message,
                                        constants.kitchenChannel,
                                        ":cake:")
            # tell general channel that cake has arrived - no gif
            self.slackMouth.publishMessage(message,
                                        constants.generalChannel,
                                        ":cake:")
            # upload the image
            self.slackMouth.postImage(fullImagePath, "#" + constants.generalChannel)
            
        return()
    
    def handleWeatherRequest(self):
        # state what the quote will do tomorrow. 
        self.ledWeather.turnOn()
        self.speechEngine.say(self.weatherService.forcast())
        self.ledWeather.turnOff()

        return()
        
    def handleCoffeeAgeRequest(self):
        # when the age of the coffee is requested, this verbally tells the user
        coffeeBrewTimeMins = 5 # time from the switch activation to coffee ready
        
        self.ledCoffeeAge.turnOn()
        
        if self.coffeeTimerFirstRun:
            self.speechEngine.say(self.quips.returnCoffeeUnknownAgeQuip())
        else:
            # say how old the coffee is
            timeNow = time.time()
            coffeeAgeMins = (timeNow-self.coffeeMadeTime)/60 - coffeeBrewTimeMins
            if coffeeAgeMins <=0:
                # coffee is still pouring
                self.speechEngine.say(self.quips.returnCoffeeBrewingQuip())
            else:
                CoffeeAgeMinsString = str(int(coffeeAgeMins))
                self.speechEngine.say("the coff ee is " + CoffeeAgeMinsString + " minutes old." )
                if coffeeAgeMins <= 60:
                    self.speechEngine.say(self.quips.returnFreshCoffeeQuip())  
                elif coffeeAgeMins >60: 
                    self.speechEngine.say(self.quips.returnOldCoffeeQuip())
                    #time.sleep(1)
                    #self.speechEngine.say("Did you know that. "+ author) 
                    #self.speechEngine.say("once said" )
                    #time.sleep(0.75)
                    #self.speechEngine.say(quote)
                    
                    time.sleep(1)
                    # self.speechEngine.say(self.weatherService.forecast())
                    # self.speechEngine.say("you can think about that while you make the coffee")
        self.ledCoffeeAge.turnOff()

        return()
