import RPi.GPIO as GPIO
import logging

class switch():
    # Common switch class used by all bots
    def __init__(self, name, pinSwitch):
        # set up GPIO channels
        self.pinSwitch = pinSwitch
        GPIO.setup(pinSwitch, GPIO.IN)  
        self.name = name
       
    def returnState(self):
        state = GPIO.input(self.pinSwitch)
        logging.debug("state of switch ", self.name, "was", state)
        return(state)
        

    