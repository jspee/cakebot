from twisted.web.server import Site
from twisted.web.static import File
from twisted.internet import reactor
import logging
from multiprocessing import Process
import constants

class server():
    
    def __init__(self):
        serverProcess = Process(target=self.runServer)
        serverProcess.daemon=True
        serverProcess.start()
        logging.debug("running server in " + constants.imageLocation)
         
    def runServer(self):
        resource = File(constants.imageLocation)
        factory = Site(resource)
        reactor.listenTCP(constants.port, factory)
        reactor.run()
        logging.debug("webserver running on port %s" %constants.port) 
        
        
