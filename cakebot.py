# simple bot for announcing kitchen activity on slack
# John Proudlock 2017
# see associated readme.md for details

# pull in libraries and set globals
from services.bot import bot
import logging
import constants

# setup logging according to build environment
if constants.PCMode:
    # run in debug mode    
    logging.basicConfig(filename=constants.logfile ,format='%(asctime)s [%(levelname)s] (%(threadName)-10s) %(message)s', level=logging.DEBUG)
    logging.debug('Cakebot started in STUB mode (for PC debug)')
else:    
    logging.basicConfig(filename=constants.logfile ,format='%(asctime)s [%(levelname)s] (%(threadName)-10s) %(message)s', level=logging.INFO)

# create & run cakebot
logging.info('\nCakebot went live')
cakebot = bot("CakeBot")
cakebot.run()