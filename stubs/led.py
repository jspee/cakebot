# switch STUB

import time
import logging

class led():
    # Common leds class
    
    def __init__(self, name, pinLED):
        logging.debug("initialised switch STUB:" + name)
        self.pinLED = pinLED
        self.name = name
        self.state = False
        self.turnOff() # set a default state
       
    def returnState(self):
        return(self.state)
    
    def turnOn(self):
        logging.debug("turn on LED for "+ self.name)
        self.state=True
    
    def turnOff(self):
        logging.debug("turn off LED for "+ self.name)
        self.state=False
    
    def oneSecondBlink (self, cycles):
        # blink function
        if cycles>0:
            logging.debug("Flash cycles: %s" %cycles)
            for i in range(0,cycles):
                self.turnOn()
                time.sleep(0.5)
                self.turnOff()
                time.sleep(0.5)
        return

    def minuteCountdown(self, minutesDuration):
        # countdown function - sets LED on for required duration 
        # The number of flashes in 1 minute indicates the number of minutes the timer has been running
        # LED flashes constantly after 1 hour has elapsed
        
        # start countdown
        logging.debug("timer - minutes: %s" %minutesDuration)
        for minute in range(0, minutesDuration):    
            self.turnOn()
            
            if minute < 60: 
                # solid 'on' sequence      
                time.sleep(60-minute)
                # blink to encode minutes passed
                self.oneSecondBlink(minute)
            else:
                # after 1 hour, blink LED for entire 60s
                self.oneSecondBlink(60)
        return   