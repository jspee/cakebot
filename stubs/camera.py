# camera STUB
import datetime
from shutil import copyfile
import logging
import constants

class camera():
    def __init__(self):
        logging.debug("initialised camera STUB")
    def takePhoto(self): 
        uniqueFilename = '{:%Y-%m-%d_%H:%M:%S_}'.format(datetime.datetime.now()) + constants.imageFileName
        print("STUB camera took picture: "+uniqueFilename)
        # as there's no camera, copy a known file and give it the unique filename
        copyfile(constants.imageLocation+constants.imageFileName, constants.imageLocation+uniqueFilename)
        
        return(uniqueFilename)
