# Cakebot Project
# John Proudlock
# Jan 2017
#
# All the constants used in the project are declared here. 
# constants aren't allowed in the python language, so having them in this module means they 
# are declared in code with an obvious name, as constants.blah

# all pins are reference using the BCM mode
# http://raspi.tv/wp-content/uploads/2013/07/Rev2-GPIO-bold.jpg

import os
import socket

# slack token name
slack_token = 'SLACK_TOKEN'

# Switch Pins
# pinWeatherSwitch = 24
pinCoffeeAgeSwitch = 24
pinCakeSwitch = 25
pinCoffeeSwitch = 23 

# LED Pins
# pinWeatherLED = 17
pinCoffeeAgeLED = 17
pinCakeLED = 18
pinCoffeeLED = 4

# timer durations
countdownValueCoffee = 30 # minutes

# network settings
hostname ='cakebot'
port=5001

# channel for notifications
kitchenChannel = 'kitchen'
generalChannel = 'general'

# filenames and paths
dirPath = os.path.dirname(os.path.realpath(__file__))
logfile = dirPath + '/cakebot.log'
imageLocation = dirPath + '/images/'
imageFileName = 'cake.jpg'
cameraFailFileName = 'camerafail.jpg'

# Deployment environment switch - running as PCmode stubs out all GPIO functions
if socket.gethostname() == hostname:
    PCMode = False
else:
    PCMode = True

