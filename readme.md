# Pi Software Config - you may need to do the following 
## Before you start
Be aware that this python project runs slackclient, a library that only runs with Python >= 3.6 compiler. 

## install pip (python package manager)
> sudo apt install python-pip

## you will need the following libraries installed
> sudo pip install slackclient

> sudo pip install twisted

> sudo pip install RPi.GPIO

> sudo pip install picamera

## you may need to complete further updates
> pip install --upgrade requests

## and store your slack token as a system variable
get you token from: https://api.slack.com/docs/oauth-test-tokens

edit /etc/environment and add the text
>SLACK_TOKEN=yourtokenhere

^ note, no spaces around the '=' 

also note, SLACK_TOKEN parameter must be visible by sudo (cakebot is run as sudo to access the GPIO hardware).
If your environment param edits are not visible the solution might be that the setting 
>Defaults env_reset 

in /etc/sudoers is resetting root's PATH defined by /etc/environment.

You should modify the file:
>/etc/sudoers

so that the existing env_reset flag is false:
>Defaults !env_reset

## enable the camera 
https://www.raspberrypi.org/documentation/usage/camera/python/README.md
> sudo raspi-config

## Install a webserver (if images are to be hosted)
> sudo apt install python-twisted-core
> sudo apt-get install python-twisted-web

## Set the pi's hostname
Hosting hardware must be a pi, so that the GPIO can be used. 
Hostname must be 
> cakebot
which is done using the command
> sudo raspi-config

if the hostname is not cakebot the software runs in debug mode (no hardware actions)

## Install espeak
> sudo apt-get install espeak python-espeak

## run the program
> python2 cakebot.py
(note that the slack client only works with python2 compilers)

## set the programme to run on boot
To get the program to run automatically on booting, use the supplied SystemD service unit configuration file.
The following commands will add the cakebot service
```
sudo cp systemd/cakebot.service /etc/systemd/service
sudo systemctl daemon-reload
sudo systemctl enable cakebot
sudo systemctl start cakebot
```

To check the service
```
sudo systemctl status cakebot
```

If you're having problems with stability of the voice synth, consider a daily reboot as a hack.  Create the
file `/etc/cron.d/cakebot` with the contents
```
55 19 * * * root /sbin/shutdown -r now
```

# Pi Hardware Config
Output is to the GPIO using the BCM mode
http://raspi.tv/wp-content/uploads/2013/07/Rev2-GPIO-bold.jpg
pins used are declared in the file contstants.py
leds used are declared in the file constants.py

# Slack setup

## slack output
channels used are defined in constants.py
https://blackpepper.slack.com/messages/kitchen/

## slack token
you'll need to enter the oauth token for your slack instance. These are generated here:
https://api.slack.com/docs/oauth-test-tokens

This can be configured on the Pi at /etc/environment

#Background reading
based on tutorial
https://realpython.com/blog/python/getting-started-with-the-slack-api-using-python-and-flask/

#Future work
make light pulse, not flash
use giffy api to get a random coffee or cake gif
hide the slack token - as it is the token is in plain text in the source code on the pi. 
(note the token is not present in the public repo).
